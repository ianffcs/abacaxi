(ns main
  (:require [datomic.api :as d]
            [io.pedestal.http :as http]
            [main.abacaxi.server :as mas]))

;; datomic system
(defn start-database! []
  (d/create-database mas/db-uri)
  (let [c (d/connect mas/db-uri)]
    @(d/transact c mas/db-schema)))

#_(start-database!)

(defn stop-database []
  (d/delete-database mas/db-uri))

;; webserver system
(defn start-webserver []
  (http/start (http/create-server mas/service-map)))

(defonce webserver (atom nil))

(defn start-webserver-dev []
  (reset! webserver
          (http/start (http/create-server
                       (assoc mas/service-map
                              ::http/join? false)))))
(defn stop-webserver-dev []
  (http/stop @webserver))

(defn restart-webserver []
  (stop-webserver-dev)
  (start-webserver-dev))
