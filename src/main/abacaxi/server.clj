(ns main.abacaxi.server
  (:require [datomic.api :as d]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]))

(def db-uri "datomic:free://localhost:4334/tasks")

(def db-schema
  [{:db/doc                "Task gid."
    :db/ident              :task/gid
    :db/valueType          :db.type/uuid
    :db/unique             :db.unique/identity
    :db/cardinality        :db.cardinality/one
    :db.install/_attribute :db.part/db}

   {:db/doc                "Task description."
    :db/ident              :task/description
    :db/valueType          :db.type/string
    :db/cardinality        :db.cardinality/one
    :db.install/_attribute :db.part/db}

   {:db/doc                "Task completion."
    :db/ident              :task/completed
    :db/valueType          :db.type/boolean
    :db/cardinality        :db.cardinality/one
    :db.install/_attribute :db.part/db}])

(def conn (d/connect db-uri))

(defn find-task! [db task-gid]
  (d/pull db '[*] [:task/gid task-gid]))

(defn tx-add-task [conn task-map]
  (-> (d/transact conn task-map)
     deref
     :db-after
     (d/pull '[*] [:task/gid (get (first task-map) :task/gid)])))

(defn tx-update-task [conn {:task/keys [gid description]}]
  (-> (d/transact
      conn
      [[:db/add [:task/gid gid] :task/description description]])
     deref
     :db-after
     (d/pull '[*] [:task/gid gid])))

(defn respond-hello [request]
  {:status 200 :body (request :body)})

(def routes
  (route/expand-routes
   #{
                                        ;["/list" :get list-tasks :route-name :list-task]
                                        ;["/add" :post add-task :route-name :add-task]
                                        ;["/complete" :put complete-task :route-name :complete-task]
                                        ;["/delete" :delete delete-task :route-name :delete-task]
     ["/greet" :post respond-hello :route-name :greet]}))

(def service-map
  {::http/routes routes
   ::http/type   :jetty
   ::http/port   3000})
