(ns test.abacaxi.server-test
  (:require [clojure.test :refer [deftest testing is]]
            [main.abacaxi.server :as mas]
            [datomic.api :as d]
            [io.pedestal.http.route :as route]))

#_(route/try-routing-for mas/routes :prefix-tree "/greet" :get)

(comment
  (deftest list-tasks)
  (deftest insert-task)
  (deftest complete-task)
  (deftest delete-task))

(deftest tx-add-task
  (let [conn        (do (d/delete-database mas/db-uri)
                        (d/create-database mas/db-uri)
                        (d/connect mas/db-uri))
        _           @(d/transact conn mas/db-schema)
        data        [{:task/gid         (d/squuid)
                      :task/description "BBBBB"
                      :task/completed   false}]
        tx-data-add (mas/tx-add-task conn data)]
    (is (=  (first  data)
            (dissoc tx-data-add :db/id)))))

(deftest tx-update-task
  (let [conn           (do (d/delete-database mas/db-uri)
                           (d/create-database mas/db-uri)
                           (d/connect mas/db-uri))
        _              @(d/transact conn mas/db-schema)
        data           [{:task/gid         (d/squuid)
                         :task/description "BBBBB"
                         :task/completed   false}]
        gid            (-> data first :task/gid)
        _tx-data-add   (mas/tx-add-task conn data)
        tx-data-update (mas/tx-update-task conn {:task/gid         gid
                                                 :task/description "AAAAA!"})
        _              (prn [:tx-data tx-data-update])]
    (is (= (-> (first data)
              (assoc :task/description "AAAAA!"))
           (dissoc tx-data-update :db/id)))))
